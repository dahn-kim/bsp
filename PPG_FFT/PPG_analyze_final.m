function finalMatrix = PPG_analyze(pathway)

% PPG_analyze - function extracting features from PPG signal
%
% Syntax:  resultsMatrix=PPG_analyze('Complete pathway to an edf file')
%
% Inputs:
%    pathway - pathway to edf file
%
% Outputs:
%    matrix with 8 columns for the following extracted features:
%       - Mean
%       - Variance
%       - Peak frequency
%       - Median frequency
%       - Mean frequency
%       - Mean inter-beat interval
%       - Variance of inter-beant intervals
%       - Skewness of distribution of inter-beat intervals
%    every row corresponds to a 60 second time window of the signal
%
% Example: 
%    resultsMatrix = PPG_analyze('s1_low_resistance_bike.edf');
%
%
% Author: ATAIE Mohamed Ali
%         BHAVSAR Hesha 
%         KHAN Junaid Akhter 
%         KIM Daeun
% October 2020;
    
    % reading in the ppg data
    [hdr , record] = edfread (pathway);
    channel = 1; % selecting channel
    
    % extracting information from header
    ppg = record(channel,:);
    fs = hdr.frequency(channel);
    yunit = hdr.units(channel);
    signaltype = hdr.label(channel);
    
    sampleCount = length(ppg);

    t = ((1:length(ppg))-1) / fs;   % time intervall for x axis

    % plotting the original PPG signal
    figure();
    set(gcf, 'Position',  [100, 100, 1300, 600]);       %definig size of window and position
    subplot(2,1,1);
    plot(t,ppg);
    xlabel('Time (s)');
    ylabel(yunit);
    title('Raw photoplethysmography signal');
    
    
    % bandpass filter from 0.5 Hz to 5 Hz (6th order butterworth)
    % initial bandpass range was taken from here: 0.5 to 20 Hz https://www.nature.com/articles/s41598-019-49092-2
    % but after correspondece we adjusted to a lower cutoff frequency of
    % 0.5 Hz to 5 Hz
    bpfilter = designfilt('bandpassfir','FilterOrder',6,...
             'CutoffFrequency1',0.5,'CutoffFrequency2',5,...
             'SampleRate',fs);      %bandpass filter is created

    ppg_filtered = filtfilt(bpfilter,ppg);  %bandpass filter is applied

    % plotting filtered PPG signal
    subplot(2,1,2);                                      % defining space of graph
    plot(t,ppg_filtered);                                       % plot graph
    title('Filtered PPG Signal')                        % define title of graph
    xlabel('Time (s)');
    ylabel(yunit);


    %segmenting
    size60 = fs * 60;   % amount of samples for 60sec frame
    segmentCount = ceil(sampleCount/size60); % counting how many 60 sec frames are created

    segmented = buffer(ppg_filtered, size60); % segmenting into 60 sec frames
    segmented = transpose(segmented);

    %initializing matrix for saving features
    resultsData = zeros(segmentCount, 8);

    % loop going through all segments and saving features in resultsData
    for i=1:segmentCount
        % calculating mean and variance
        resultsData(i,1) = mean(segmented(i,:));
        resultsData(i,2) = var(segmented(i,:));
        
        % smoothening and preprocessing signal for further steps
        ppg_current = normalize(segmented(i,:));
        ppg_current = smoothdata(ppg_current);
        
        %   initial approach for frequency domain with fft
        %   but due to a faulty spectrum, we commited to using
        %   the function 'periodogramm' (spectral density)
        %   fc = (fft(ppg_current,ppg_length));
        %   f = abs(fc);
        %   figure()
        %   plot(f);
        
        %   frequency domain features established
        [pxx,f] = periodogram(ppg_current);
        f = f.*(fs/2); % convert normalized frequency to hertz

        [y,x] = max(pxx);
        
        resultsData(i,3) = f(x);
        resultsData(i,4) = meanfreq(ppg_current,fs);
        resultsData(i,5) = medfreq(ppg_current,fs);
        
        % using findpeaks to find peaks of beats
        [pks,locs]=findpeaks(ppg_current, 'MinPeakHeight',0.5, 'MinPeakDistance',110);

        % loop calculating intervals from locations
        % and dividing by fs to have result in seconds
        for j = 2:length(locs)              
            rtor(j)=(locs(j)-locs(j-1))/fs;    
        end

        % calculating mean interval, variance and skewness
        meanInterval = mean(rtor);
        varInterval = var(rtor);
        skewInterval = skewness(rtor);
        
        % saving data in matrix
        resultsData(i,6) = meanInterval;
        resultsData(i,7) = varInterval;
        resultsData(i,8) = skewInterval;


    end

    % passing final matrix:
    finalMatrix = resultsData;

    
end