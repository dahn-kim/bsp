%% IA assignment - dicom toolbox version
% Reading DICOM files via Matlab
% 1. The definition of a dicom image file from the standard
% 2. fread in matlab into a pgm file, manipulate the image by adding a
% line or markers to the image
% Or using the matlab's image processing toolbox(dicomread)
% 


%% reading and open a DICOM image 


info=dicominfo('CT-MONO2-16-chest');
bild = dicomread(info);
figure(1)
imshow(bild,[]);
bild=double(bild); % changing the data type to double 
title([info.StudyDescription, info.ContentDate])
hold on;


%% Creating markers for the windowing

corners=[]; % creating two plotting marker 
n = 2;
for k=1:n
[x,y] = ginput(1); % cursor positioning
plot(x,y,'Marker','o','Color',[1 0 0],'MarkerSize',10)
corners=[corners [x y]'];
end
corners=round(corners);


%% image windowing

figure(2);
colormap(gray(64));
% windowing the part of the image selected using the markers
bildteil=bild(min(corners(2,:)):max(corners(2,:)),min(corners(1,:)):max(corners(1,:)));
image(bildteil);
title([info.StudyDescription, ' windowed'])


%% image adjustment using mean and 
%  std for deeper look of the windowed part
figure(3)
colormap(gray(64));
m = mean(bildteil(:));
s = std(bildteil(:));
new_img=(63*(bild-(m-3*s))/(6*s)+1);
image(new_img)
title('adjusted')
axis('equal')
axis('off')

%% Saving as a pgm file 
graymap=colormap(gray(64));
toPGM=imshow(new_img,graymap);
saveas(toPGM,'converted.pgm')
savedPGM=imread('converted.pgm');

%% function version of reading DICOMs
%bild=readFile('CT-MONO2-16-chest'); %add DICOM a file you wish to read
%function bild= readFile(file_name)
%    info=dicominfo(file_name);
%    bild = dicomread(info);
%    figure(1)
%    title(info.StudyDescription)
%    imshow(bild,[]);
%    bild=double(bild); % changing the data type to double 
%end
