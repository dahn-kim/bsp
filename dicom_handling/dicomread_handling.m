%% IA assignment - dicom toolbox version
% Reading DICOM files via Matlab
% 1-1) The definition of a dicom image file from the standard
% a) fread in matlab into a pgm file, manipulate the image by adding a
% line or markers to the image
% b) matlab's image processing toolbox(dicomread)
function dicomread_handling(file)
    info=dicominfo(file);
    Y = dicomread(info);
    figure
    imshow(Y,[]);
    %writing into a pgm file
    
    
    corners=[];
    for k=1:2
    [x,y] = ginput(1);
    plot(x,y,'Marker','o','Color',[1 0 0],'MarkerSize',10)
    corners=[corners [x y]'];
    end
    corners=round(corners);
    
    figure(2);
    colormap(gray(64));
    bildteil=Y(min(corners(2,:)):max(corners(2,:)),min(corners(1,:)):max(corners(1,:)));
    image(bildteil)  
    
    m=mean(bildteil(:));
    s=std(bildteil(:));
    image( 63*(bild-(m-3*s))/(6*s)+1 )
    axis('equal')
     
    baseFileName=sprintf('testing.pgm');
    imwrite(bildteil,baseFileName);
end