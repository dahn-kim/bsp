% read dicom file

%read_dicom_file
%dicomread_handling

bild=readFile('CT-MONO2-8-abdo');
hold on;

% window the image

corners=[];
n = 2;
for k=1:n
[x,y] = ginput(1);
plot(x,y,'Marker','o','Color',[1 0 0],'MarkerSize',10)
corners=[corners [x y]'];
end
corners=round(corners);

figure(2);
colormap(gray(64));
bildteil=bild(min(corners(2,:)):max(corners(2,:)),min(corners(1,:)):max(corners(1,:)));
image(bildteil)

figure(3)
colormap(gray(64));
m = mean(bildteil(:));
s = std(bildteil(:));
image(63*(bild-(m-3*s))/(6*s)+1)
axis('equal')
axis('off')

map=colormap(gray(64));
imwrite(bildteil,'PGM_1.pgm'); % exporting quality must be improved


function bild= readFile(file_name)
    info=dicominfo(file_name);
    bild = dicomread(info);
    figure(1)
    imshow(bild,[]);
    bild=double(bild); % changing the data type to double 
end
