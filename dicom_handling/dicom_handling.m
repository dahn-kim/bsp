%% IA assignment - fread version
% Reading DICOM files via Matlab
% 1-1) The definition of a dicom image file from the standard
% a) fread in matlab into a pgm file, manipulate the image by adding a
% line or markers to the image
% b) matlab's image processing toolbox(dicomread)
%%
% 2d imaging -> f(x,y) = p
% 3d imaging -> f(x,y,z) = p

%% implementation

fp = fopen('CT-MONO2-16-brain');
fseek(fp, -512^2*2, 'eof'); % 512^2 is assumed for CT imaging, colour depth of two bytes
img = zeros(512); % filling 0s with 512x512 table
img(:)=fread(fp,512^2,'int16'); 
colormap(gray(128))
img = img'; % transpose without conjugation
subplot(2,1,1);
image(img)
axis('equal')
axis('off')

