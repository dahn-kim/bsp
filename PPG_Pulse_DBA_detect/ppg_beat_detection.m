
function finalMatrix = ppg_beat_detection(ppg_signal, sampling_rate)
%%  INSTRUCTIONS 
% Please copy and paste these scripts in order to run this file:
% [hdr, data1] = edfread( 'your ppg file', 'targetSignals', { 'wrist_ppg'});
% ppg = data1(1,:);
% fs = hdr.frequency(1);
% finalMatrix = ppg_beat_detection(ppg, fs);

% attached you will find our modified DBA.m

%%
% PPG beat detection - function to detect a pulse using quality index calculation by Papini's new method
%
% Syntax:  finalMatrix = ppg_beat_detection(ppg_signal, sampling rate)
%
% Inputs:
%    PPG signal as column array 
%     sampling rate in Hz 
%
% Outputs:
%   Matrix with timing, amplitude, and quality index for each detected beat 
%   
%
% Example: 
%    finalMatrix = ppg_beat_detection(ppg, fs)
%
%
% Author: ATAIE Mohamed Ali 
%         BHAVSAR Hesha
%         KHAN Junaid Akhter 
%         KIM Daeun 
% November 2020;


ppg = ppg_signal;
fs=sampling_rate;
t = ((1:length(ppg)) - 1) / fs; % time intervall for x axis
range = [0, t(end)];

%%
% ------------------  pre processing, creating two versions of the signal  ---------------------       
ppg_filtered225 = bandpass(ppg,[0.4 2.25],fs);  %bandpass from 0.4 to 2.25 Hz
ppg_filtered10 = bandpass(ppg,[0.4 10],fs);  %bandpass from 0.4 to 10 Hz

% plotting filtered PPG signal
% figure(2);                                   % defining space of graph
% set(gcf, 'Position',  [100, 100, 1300, 600]);       %definig size of window and position
% subplot(2,1,1);
% plot(t,ppg_filtered225,t,ppg_filtered10);                 % plot graph
% legend('ppg 2.25Hz','ppg 10Hz');
% title('Filtered PPG Signal')                        % define title of graph
% xlabel('Time (s)');
% ylabel(yunit);

%%
%------------------   segmentation & beat localization  --------------------------

[pks,locs]=findpeaks(-ppg_filtered225); % minima in 225 signal - start and end of each pulse
%[pks2,locs2]=findpeaks(ppg_filtered225);    % peak in 225 signal  - used for segmenting

% creating matrices to fill with data
pp10Start =  []; % for saving pp10 segments
pp10End =  [];
pp10beatLoc = [];
pp10 = zeros(length(locs),512);
pp10Sizes = [];  % for saving lengths of pp10s
allGradient = zeros(length(locs),1); % for saving the max gradients of pp10s


for f = 1:length(locs)
   
   % marking detected minima and peaks in plot
   hold on
   %plot(locs(f)/fs,ppg_filtered225(locs(f)),'r*');  % marking locations for minima
   

   
   if f ~= length(locs)
       
       % next peak in 225 signal  - used for segmenting (between current
       % minima and next minima)
       [pks3,locs3]=findpeaks(ppg_filtered225(locs(f):locs(f+1))); 
       
       % compensating relativ index of findpeaks to absolute index of whole
       % signal
       peakLocGlobal=locs(f)+locs3(1); %first peak is relevant
       % marking locations for peaks in 225
       %plot(peakLocGlobal/fs,ppg_filtered225(peakLocGlobal),'b*');
       
       % evaluating starting point of window ( between minima and peak of
       % 225) - minima between those two points is the starting point
       [pks4,locs4]=findpeaks(-ppg_filtered10(locs(f):peakLocGlobal)); 
       % compensating relativ index of findpeaks to absolute index of whole
       % signal
       if size(locs4) >= 1
            startOfPP=locs(f) + locs4(1);
            % marking the actual starting point
            %plot(startOfPP/fs,ppg_filtered10(startOfPP),'g*');
       end
     
       % evaluating ending point (finding minima between peak and next
       % minima + 1/4)
       lastEndingGlobal = round(locs(f+1) + ((locs(f+1)-locs(f))/4));
       % finding minima for ending point
       if lastEndingGlobal > length(ppg_filtered10)
           lastEndingGlobal = length(ppg_filtered10);
       end
       [pks5,locs5]=min(ppg_filtered10(peakLocGlobal:lastEndingGlobal));
       endOfPP = peakLocGlobal + locs5;
       %plot(endOfPP/fs,ppg_filtered10(endOfPP),'y*');
       
      
           
       windowSize = endOfPP - startOfPP;
       
       
       if ((windowSize > (fs*0.5))&& (windowSize < (fs*1.5)))
           pp10Start =  [pp10Start;startOfPP];
           pp10End =  [pp10End;endOfPP];
           pp10Sizes = [pp10Sizes;windowSize];
           pp10(f,1:windowSize+1) = ppg_filtered10(startOfPP:endOfPP);
           
           allGrad = gradient(ppg_filtered10(startOfPP:endOfPP));
           [maxGrad,maxGradLoc] = max(allGrad);
           globalGrad = startOfPP + maxGradLoc;
           pp10beatLoc = [pp10beatLoc;globalGrad];
           
           %plot(globalGrad/fs,ppg_filtered10(globalGrad),'b*');
           
           
       end    
   end
end

%%
% -------------------------------pulse normalisation.....................................................
%  calculate normalised version of pp10 (1). PPtemp, (2). PPpqi

% all amplitudes
ppAmplitude = []; % saving amplitudes
ppAmplitude_position = []; %>>>>>>>>>saving the sample index of the amplitude
allPPtemps = zeros(length(locs),512); % making spac
pp10Segment = [];
maxpp10Size = max(pp10Sizes)+1;
pp10SizeAve = round(mean(pp10Sizes));
%PPpqi = zeros(length(pp10Start),maxpp10Size);
%PPtemp = zeros(length(pp10Start),maxpp10Size);

for t = 1:length(pp10Start)
    
    pp10Segment= ppg_filtered10(pp10Start(t):pp10End(t));
    
    amplitude = max(pp10Segment) - min(pp10Segment);% eq7 for calculate amplitude
    amplitude = abs(amplitude); % taking abs of amplitude
    
    ppAmplitude = [ppAmplitude;amplitude]; % saving amplitude
    ppAmplitude_position = [ppAmplitude_position; (find(pp10Segment == max(pp10Segment))+pp10Start(t))]; %>>>>>>find the global index of the peak/amplitude
    %figure;
    %plot(pp10Segment);
   
    shift = (max(pp10Segment) + min(pp10Segment) )/2; % calculate shift for eq 8
    %for i =1:pp10Sizes(t)%saving the data in segments,
    
    PPtemp{t,:}= (pp10Segment- shift)/amplitude;  % implementing eq 9, first normalisation of PP10
    
    %allPPtemps = [allPPtemps;PPtemp];
    PPpqi{t,:} = pp10Segment - shift ; % implementing eq 10, second normalisation of PP10
    
end

%%
%-------------------   Amplitude correction factor   --------------
%First, the algorithm removes from the amplitude time series all elements
%that have a value 50% higher or lower than the previous or the following value
%Nt = length(pp10_amplitude_array);

newAmplitude = []; % correct amplitudes 
newtime = [];

% clipping the amplitude array for the defined condition
for i=1:length(pp10Start)
    if  i == 1 && ppAmplitude(i)/ppAmplitude(i+1) < 1.5 && ppAmplitude(i)/ppAmplitude(i+1) > 0.5 % comaprision between amplitude
       
       newAmplitude =  [newAmplitude;ppAmplitude(i)];
       newtime = [newtime;ppAmplitude_position(i)];
      %%%%%% newtime(i)=pp10_amplitude_array(i)/a column for time??
    
    
    elseif i<length(pp10Start) && ppAmplitude(i)/ppAmplitude(i+1) < 1.5 && ppAmplitude(i)/ppAmplitude(i+1) > 0.5 && ppAmplitude(i)/ppAmplitude(i-1) < 1.5 && ppAmplitude(i)/ppAmplitude(i-1) > 0.5   % comaprision between amplitude
        
         newAmplitude =  [newAmplitude;ppAmplitude(i)];
         newtime = [newtime;ppAmplitude_position(i)]; %the global sample index of a local max
         
         
    elseif i==length(pp10Start) && ppAmplitude(i)/ppAmplitude(i-1) < 1.5 && ppAmplitude(i)/ppAmplitude(i-1) > 0.5
        newAmplitude =  [newAmplitude;ppAmplitude(i)];
        newtime = [newtime;ppAmplitude_position(i)];
    end
end 


%----------------  interpolation   ------------------

% the clipped amplitude time series is interpolated at 4 Hz using a cubic spline interpolation 
xx= 1:64:length(ppg); %interpolationfrequency fs/4=64 >> 4hz and the limts of x-axis
pp = csapi(newtime,newAmplitude);%calculating the pp 
yy = ppval(pp,xx); %calculating the particular y values for the defined x (interpolation of clipped time series)
  
 %--------------filtering the signal with low pass filter---------------------

 fc = 1.5;
[b,a] = butter(3,fc/(4/2));
Amplitude_timeseries_filtered= filtfilt(b,a,yy);  % for zero phase

 %----------------- resampling ----------------------------
Amplitude_timeseries = timeseries(Amplitude_timeseries_filtered, xx); %defining the time series
clean_amps = resample(Amplitude_timeseries,ppAmplitude_position); %resampling the signal in original amplitude location
cleaned_Amplitude_timeseries = clean_amps.data;
%%

%--------------------------------- Template creation -------------------

% ploting the cluster of each pulse
subplot(2,1,1);
for j=1:length(pp10Start)
       plot(PPtemp{j,:})
       if j == 1
           hold on
       end
end
hold off
title('cluster')

% applying dynamic time warping barycenter averaging
seq = transpose(PPtemp);
average = DBA(seq(:));

% Applying the lowpass filter to averaged template

fc = 10; % cutoff freq
[b,a] = butter(3,fc/(fs/2));
%freqz(b,a);
dataOut=filter(b,a,average);
subplot(2,1,2);
title('DBA filtered')
plot(dataOut);

%   Temp _ad calculation 
Temp_ad = zeros(length(clean_amps.data),length(dataOut));

for k = 1:length(clean_amps.data)
    
    Temp_ad(k,1:(length(dataOut))) = cleaned_Amplitude_timeseries(k) * dataOut;      % <- Temp_ad
    
end

%%
%--------------- PPpqi adaptaion  ------------

maxwarplength = ceil(fs*0.3);  % constraint 1: maximum time warping to 0.35s

for z = 1:length(clean_amps.data)
    current_temp = Temp_ad(z,:);
    current_pqi = PPpqi{z};
    [maxdist,ix,iy] = dtw(current_temp,current_pqi,maxwarplength);   % DTW with constraint 1
    Temp_ad_warped{z} = current_temp(ix);   % Temp_ad was warped to match the matrix size of PP_warped for calculation
    PP_warped{z} = current_pqi(iy);
end

% applying before created butter filter
for x = 1:length(clean_amps.data)
    PP_warped_filtered{x} =filter(b,a,PP_warped{x});
end

% ----------------------- PQI calculation -----------------


finalPQI = zeros(1,length(clean_amps.data));

for h = 1:length(clean_amps.data)
    
    UP = [];
    MP = [];
    
    currentPP_warped = PP_warped_filtered{h};
    currentTemp_warped = Temp_ad_warped{h};
% Compare between PP_pqi and Temp_ad in PPs_warped 
% Find indices of each sample of PP_warped with a difference 
% higher than 10% related to Temp_ad=>UP(unmatched points)
% MP(matched points)  

    for p = 1:length(currentPP_warped)

        diff = abs((currentPP_warped(p) - currentTemp_warped(p))/currentTemp_warped(p));

        if diff > 0.1
            UP = [UP;p];
        else
            MP = [MP;p];
        end

    end

    Nmp=length(MP);
    Nup=length(UP);
    Npercent=Nmp/(Nmp+Nup); % the percentage of matching points 

    %rmsSum = zeros(1,maxpp10Size);
    %summand = zeros(1,maxpp10Size);
    
% Root mean square  of the unmatched points in respect to Temp_Ad
    
    if Nup == 0
        RMSEup = 0;
    else
        rmsSum=0;
        for e = 1:Nup
            currentUPindex = UP(e);
            currentTempAd = currentTemp_warped(currentUPindex);
            currentPPWarp = currentPP_warped(currentUPindex);
            summand = (currentTempAd - currentPPWarp)^2;
            rmsSum = rmsSum + summand;
        end

        RMSEup = sqrt((rmsSum)/Nup);
    end
    
    tempAdMax = max(Temp_ad(h,:));
    tempAdMin = min(Temp_ad(h,:));
    
  %  Temp_Ad amplitude normalisation + correction of the error
    
    RMSEnorm = RMSEup/(tempAdMax-tempAdMin);
    
   %  Scoring the quality index between 0(low) -1(high)
   
    RMSEratio = RMSEnorm/Npercent;
    
    if RMSEratio > 1
        finalPQI(h) = 0;
    else
        finalPQI(h) = 1 - RMSEratio;
    end

end

bufferMatrix = zeros(3,length(pp10beatLoc));
bufferMatrix(1,:) = pp10beatLoc/fs;
bufferMatrix(2,:) = ppAmplitude;
bufferMatrix(3,:) = finalPQI;

finalMatrix = bufferMatrix;

end
 

   
    
    
    
    
    
